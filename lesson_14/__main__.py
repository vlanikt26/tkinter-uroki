import sqlite3
import os
from contacts_repository import ContactsRepository
from contacts_view import ContactsView
from contacts_controller import ContactsController


def main():
    db_path = os.path.join(os.getcwd(), "lesson_14", "contacts.db")
    with sqlite3.connect(db_path) as conn:
        repo = ContactsRepository(conn)
        view = ContactsView()
        ctrl = ContactsController(repo, view)

        view.set_ctrl(ctrl)
        ctrl.start()


if __name__ == "__main__":
    main()
