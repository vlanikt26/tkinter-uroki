import tkinter as tk


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        menu = tk.Menu(self)
        file_menu = tk.Menu(menu, tearoff=0)

        file_menu.add_command(label="Новый файл")
        file_menu.add_command(label="Открыть")
        file_menu.add_separator()
        file_menu.add_command(label="Сохранить")
        file_menu.add_command(label="Сохранить как...")

        menu.add_cascade(label="Файл", menu=file_menu)
        menu.add_command(label="О программе")
        menu.add_command(label="Выйти", command=self.destroy)
        self.config(menu=menu)


if __name__ == "__main__":
    app = App()
    app.mainloop()
