import time
import json
import random
from http.server import HTTPServer, BaseHTTPRequestHandler


class RandomRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        # Имитация задержки
        time.sleep(3)

        # Добавляем заголовки ответа
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

        # Добавляем тело ответа
        body = json.dumps({'random': random.random()})
        self.wfile.write(bytes(body, "utf8"))


def main():
    """Запускает HTTP-сервер на порту 8090"""
    server_address = ('', 8090)
    httpd = HTTPServer(server_address, RandomRequestHandler)
    httpd.serve_forever()


if __name__ == "__main__":
    main()